import { JobData } from "@/types";
import React from "react";
import styles from "./JobDetail.module.css";

export const jobDetails: JobData[] = [
  {
    slug: "teaching-position-at-schools",
    title:
      "Teaching Positions at International and Public Schools in Hanoi and Surrounding Areas",
    location:
      "Hanoi, Hai Duong City, and Bac Giang City, all within approximately 50 kilometers of Hanoi, the capital of Vietnam.",
    description: {
      studentDemographics:
        "Teach young learners aged 2 to 15 years old, encompassing kindergarten, primary, and secondary school students.",
      classSize:
        "Manage larger class sizes ranging from 20 to 35 students, tailored to provide an engaging and structured learning environment.",
      curriculumSupport:
        "A well-developed curriculum and comprehensive teaching materials, including textbooks, are provided. Each class is supported by a teacher assistant to ensure effective classroom management and support.",
      workingSchedule:
        "The teaching schedule is primarily from Monday to Friday, covering both mornings and afternoons, with occasional Saturday morning sessions.",
      workHours:
        "Teachers are expected to work a maximum of 100 hours per month, allowing for a balanced and sustainable workload.",
    },
    benefits: {
      professionalDevelopment:
        "Teachers receive a free training course to enhance their teaching skills and adapt to the educational environment of international and public schools.",
      competitiveCompensation:
        "A net monthly salary ranging from $1,450 to $1,550, reflecting the competitive nature of the role within the education sector.",
      accommodationAndTransportation:
        "Complimentary accommodation and transportation are provided, ensuring convenience and comfort for teachers.",
    },
    requirements: {
      qualifications:
        "Applicants must be native English speakers with a minimum of a three-year degree from an accredited institution.",
      certification:
        "A TEFL (Teaching English as a Foreign Language) certificate is required.",
      experience:
        "No prior teaching experience is necessary, making this an excellent opportunity for individuals looking to gain experience in a professional teaching environment.",
    },
  },
  {
    slug: "teaching-position-at-language-centers",
    title:
      "Teaching Positions at Language Centers in Hanoi and Surrounding Areas",
    location: "Bac Ninh, Bac Giang, Nam Dinh, Phuc Yen City...",
    description: {
      studentDemographics:
        "Teach young learners aged 2 to 15 years old, with a primary focus on students aged 6 to 12.",
      classSize:
        "Manage small class sizes ranging from 10 to 15 students, allowing for a more personalized and effective teaching experience.",
      curriculumSupport:
        "A comprehensive curriculum and teaching materials, including textbooks and resources, are provided. Each class is also supported by a teacher assistant to facilitate the learning process.",
      workingSchedule:
        "The teaching schedule primarily consists of evening and weekend classes, with one designated day off per week.",
      workHours:
        "Teachers are expected to work a maximum of 100 hours per month, ensuring a balanced workload.",
    },
    benefits: {
      professionalDevelopment:
        "Teachers receive a free training course designed to equip them with the necessary skills and knowledge to excel in their roles.",
      competitiveCompensation:
        "A net salary ranging from $1,400 to $1,500 per month, offering a competitive rate for the region.",
      accommodationAndTransportation:
        "Complimentary accommodation and transportation are provided, ensuring a comfortable living and working experience.",
    },
    requirements: {
      qualifications:
        "Applicants must be native English speakers with a minimum of a three-year degree from an accredited institution.",
      certification:
        "TEFL (Teaching English as a Foreign Language) certificate is required.",
      experience:
        "No prior teaching experience is necessary, making this an ideal opportunity for individuals looking to begin their teaching careers abroad.",
    },
  },
  {
    slug: "online-teaching",
    title: "Online Teaching",
    location: "Online",
    description: {
      studentDemographics:
        "Teach students aged 2 to 15 years old, with a primary focus on students aged 6 to 12.",
      classSize:
        "Manage large class sizes ranging from 20 to 35 students, allowing for an engaging and structured learning environment.",
    },
    benefits: {
      professionalDevelopment:
        "Teachers receive a free online training course designed to equip them with the necessary skills and knowledge to excel in their roles.",
      competitiveCompensation:
        "A net salary ranging from $1,400 to $1,500 per month, offering a competitive rate for the region.",
      accommodationAndTransportation:
        "Complimentary accommodation and transportation are provided, ensuring a comfortable living and working experience.",
    },
    requirements: {
      qualifications:
        "Applicants must be native English speakers with a minimum of a three-year degree from an accredited institution.",
      certification:
        "TEFL (Teaching English as a Foreign Language) certificate is required.",
      experience:
        "No prior teaching experience is necessary, making this an ideal opportunity for individuals looking to begin their teaching careers online.",
    },
  },
];

const JobDetail = ({ slug }: { slug: string }) => {
  const dataDetail: any = jobDetails.find((job) => job.slug === slug);
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h1 className={styles.title}>{dataDetail.title}</h1>
      </div>

      <div className={styles.content}>
        <div className={styles.section}>
          <h2 className={styles.subtitle}>Location:</h2>
          <p>{dataDetail.location}</p>
        </div>

        <div className={styles.section}>
          <h2 className={styles.subtitle}>Job Description:</h2>
          <ul>
            {Object.values(dataDetail.description).map((item: any, index) => (
              <li key={index}>{item}</li>
            ))}
          </ul>
        </div>

        <div className={styles.section}>
          <h2 className={styles.subtitle}>Benefits:</h2>
          <ul>
            {Object.values(dataDetail.benefits).map((item: any, index) => (
              <li key={index}>{item}</li>
            ))}
          </ul>
        </div>

        <div className={styles.section}>
          <h2 className={styles.subtitle}>Requirements:</h2>
          <ul>
            {Object.values(dataDetail.requirements).map((item: any, index) => (
              <li key={index}>{item}</li>
            ))}
          </ul>
        </div>

        <div className={styles.applyButtonContainer}>
          <a
            href="https://www.facebook.com/anhngu.2g"
            target="_blank"
            className={styles.applyButton} rel="noreferrer"
            style={{ textDecoration: "none" }}
          >
            Apply Now
          </a>
        </div>
      </div>
    </div>
  );
};

export default JobDetail;
