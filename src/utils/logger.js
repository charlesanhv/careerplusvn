/* eslint-disable no-console */
// utils/logger.js
const isProduction = process.env.NODE_ENV === 'production';

/**
 * @param {any[]} messages
 */
export function log(...messages) {
  if (!isProduction) {
    console.log(...messages);
  }
}

/**
 * @param {any[]} messages
 */
export function logError(...messages) {
  if (!isProduction) {
    console.error(...messages);
  }
}
