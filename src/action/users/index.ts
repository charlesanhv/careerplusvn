"use server";

import { UserData } from "@/types";
import { log, logError } from "@/utils";
import { fetchAPI } from "@/utils/fetchUtil";
import { revalidateTag } from "next/cache";

export const handleCreateUserAction = async (data: UserData) => {
  try {
    // Use fetchAPI instead of fetch directly
    const result = await fetchAPI("http://localhost:8000/users", "POST", data);
    log("data_created", data, result);
    revalidateTag("list-users");
    return result;
  } catch (error) {
    logError("Error creating user:", error);
    // throw error; // Further error handling or logging can be implemented here
  }
};

export const handleGetUsersAction = async (
  page: string,
  limit: string | number
) => {
  try {
    // Use fetchAPI instead of fetch directly
    const result = await fetchAPI(
      `http://localhost:8000/users?_page=${page}&_limit=${limit}`,
      "GET",
      undefined,
      {
        tags: ["list-users"],
        revalidate: 3600,
      }
    );
    return result;
  } catch (error) {
    logError("Error creating user:", error);
    // throw error; // Further error handling or logging can be implemented here
  }
};

export const handleUpdateUserAction = async (data: any) => {
  try {
    // Use fetchAPI instead of fetch directly
    const result = await fetchAPI(
      `http://localhost:8000/users/${data.id}`,
      "PUT",
      data
    );
    log("data_updated", data, result);
    revalidateTag("list-users");
    return result;
  } catch (error) {
    logError("Error creating user:", error);
    // throw error; // Further error handling or logging can be implemented here
  }
};

export const handleDeleteUserAction = async (data: any) => {
  try {
    // Use fetchAPI instead of fetch directly
    const result = await fetchAPI(
      `http://localhost:8000/users/${data.id}`,
      "DELETE",
      data
    );
    log("data_deleted", data, result);
    revalidateTag("list-users");
    return result;
  } catch (error) {
    logError("Error creating user:", error);
    // throw error; // Further error handling or logging can be implemented here
  }
};
