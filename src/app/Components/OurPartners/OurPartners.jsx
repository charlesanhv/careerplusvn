import styles from "./OurPartners.module.css";
import Image from "next/image";
import Marquee from "react-fast-marquee";

// Danh sách đối tác
const PARTNERS = [...Array(11).keys()].map((i) => ({
  src: `/images/partner/${i + 1}.jpg`,
  alt: `Partner ${i + 1}`,
}));

const OurPartners = () => {
  return (
    <div className={styles.out_partner}>
      <h2 className={styles.out_partner_title}>Our Partners</h2>
      <div className={styles.out_partner_marquee}>
        <div className={styles.out_partner_list}>
          <Marquee>
            {PARTNERS.map((partner, index) => (
              <div key={index} className={styles.out_partner_item}>
                <Image
                  src={partner.src}
                  alt={partner.alt}
                  width={150}
                  height={150}
                />
              </div>
            ))}
          </Marquee>
        </div>
      </div>
    </div>
  );
};

export default OurPartners;
