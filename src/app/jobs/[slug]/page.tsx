import JobDetail, { jobDetails } from "@/app/Components/JobDetail/JobDetail";


export const generateMetadata = ({ params }: { params: { slug: string } }) => {
  const dataDetail = jobDetails.find((job) => job.slug === params.slug);

  if (!dataDetail) {
    return {
      title: "Job not found",
      description: "The requested job could not be found.",
    };
  }

  return {
    title: dataDetail.title,
    description: dataDetail.description.studentDemographics,
  };
};

export default function JobDetailPage({
  params,
}: {
  params: { slug: string; channel: string };
}) {
  return <JobDetail slug={params.slug} />;
}
