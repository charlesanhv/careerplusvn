import {
  dataBogDetails,
  TAG_CLOUD,
} from "@/constanst/constanst";
import styles from "./BlogSidebar.module.css";
import Image from "next/image";
import Link from "next/link";

const BlogSidebar = () => {
  return (
    <div className={styles.blog_right_side_bar}>
      <div className={styles.blog_right_side_bar_wrapper}>
        {/* <div className={styles.side_bar_item_wrapper}>
          <div className={styles.side_bar_input_container}>
            <input
              placeholder="Search Keyword"
              className={styles.side_bar_input}
            />
            <div className={styles.side_bar_btn}>Search </div>
          </div>
        </div> */}

        {/* <div className={styles.side_bar_item_wrapper}>
          <div className={styles.sider_bar_item_title}>Category</div>

          {BlOG_CATAGORIES.map((item, index) => {
            return (
              <div className={styles.blog_catagory_wrapper} key={index}>
                <a className={styles.blog_catagory_content}>{`${item.name}(${
                  Number(item.number) < 10
                    ? `0${Number(item.number)}`
                    : `${Number(item.number)}`
                })`}</a>
              </div>
            );
          })}
        </div> */}

        <div className={styles.side_bar_item_wrapper}>
          <div className={styles.sider_bar_item_title}>Recent Post</div>

          {dataBogDetails.map((item, index) => {
            return (
              <div className={styles.recent_post_item} key={index}>
                <Image
                  alt="image"
                  src={item.listImages?.[Math.floor(Math.random() * item.listImages?.length)]?.url}
                  className={styles.recent_post_img}
                  width={222}
                  height={222}
                />
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <Link href={`/blog/${item.slug}`} className={styles.blog_catagory_content}>{item.title}</Link>
                  <span className={styles.time}>{new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(new Date())}</span>
                </div>
              </div>
            );
          })}
        </div>

        <div className={styles.side_bar_item_wrapper}>
          <div className={styles.sider_bar_item_title}>Tag Clouds</div>
          <div className={styles.tag_cloud_wrapper}>
            {TAG_CLOUD.map((item, index) => {
              return (
                <a className={styles.tag_cloud_content} key={index}>
                  {item}
                </a>
              );
            })}
          </div>
        </div>
{/* 
        <div className={styles.side_bar_item_wrapper}>
          <div className={styles.sider_bar_item_title}>Instagram Feeds</div>

          <div className={styles.instagram_feed_wrapper}>
            {Instagram.map((item, index) => {
              return (
                // <div className={styles.instagram_feed_item} key={index}>
                <Image
                  alt="image"
                  src={item.image}
                  className={styles.instagram_feed_img}
                  key={index}
                  width={222}
                  height={222}
                />
                // </div>
              );
            })}
          </div>
        </div> */}

        <div className={styles.side_bar_item_wrapper}>
          <div className={styles.sider_bar_item_title}>Newsletter</div>

          <div
            style={{ display: "flex", flexDirection: "column", gap: "10px" }}
          >
            <input
              placeholder="Enter email"
              className={styles.side_bar_input}
            />
            <div className={styles.sub_btn}>Subscribe </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlogSidebar;
