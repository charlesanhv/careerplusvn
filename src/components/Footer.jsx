import React from "react";
import styles from "./Footer.module.css";
import clsx from "clsx";
import Image from "next/image";

const FOOTER_ITEMS = [
  {
    title: "Quick Links",
    body: [
      { name: "About", link: "/about" },
      { name: "Job Offers", link: "/jobs" },
      { name: "Blogs", link: "/blog" },
    ],
  },
  {
    title: "Programs",
    body: [
      {
        name: "Teaching at schools",
        link: "/jobs/teaching-position-at-schools",
      },
      {
        name: "Teaching at centers",
        link: "/jobs/teaching-position-at-language-centers",
      },
      { name: "Online teaching", link: "/jobs/online-teaching" },
    ],
  },
  {
    title: "Resources",
    body: [
      { name: "FAQ", link: "/faq" },
      { name: "Contact Us", link: "/contact" },
    ],
  },
];
const Footer = () => {
  return (
    <div className={styles.footer_container}>
      <div className="container">
        <div className={styles.footer_content}>
          <div className={clsx(styles.footer_content_wrapper, "row")}>
            {/* <div className={styles.wrapper}>
                <a href="/" className={styles.wrapper_logo}>
                  <Image
                    className={styles.logo}
                    src="/images/growgreen-logo.png"
                    alt="logo"
                    width={171}
                    height={49}
                  />
                  <div className={styles.wrapper_logo_text}>
                    <span>GrowGreen</span>
                    <span>VietNam</span>
                  </div>
                </a>
              </div> */}
            {FOOTER_ITEMS.map((item, index) => (
              <div key={index} className={styles.footer_item_wrapper}>
                <div className={styles.footer_item}>
                  <div className={styles.footer_item_title}>{item.title}</div>
                  {item.body.map((item, index) => (
                    <div key={index} className={styles.footer_item_body}>
                      <a href={item.link} className={styles.footer_item_link}>
                        {item.name}
                      </a>
                    </div>
                  ))}
                </div>
              </div>
            ))}

            <div className={styles.footer_news_letter}>
              {/* <div className={styles.footer_item}> */}
              <div className={styles.footer_item_title}>Newsletter</div>
              <div className={styles.footer_item_body}>
                <div className={styles.footer_item_link}>
                  Subscribe newsletter to get updates.
                </div>
                <div className={styles.footer_news_letter_input_wrapper}>
                  <input
                    className={styles.footer_news_letter_input}
                    type="email"
                    placeholder="Enter your email"
                  ></input>
                  <div className={styles.footer_news_letter_button}>
                    <Image
                      unoptimized
                      src="/images/Icon-send.svg"
                      alt="image"
                      className="img"
                      width={20}
                      height={20}
                    />
                  </div>
                </div>
              </div>
              {/* </div> */}
            </div>
          </div>
        </div>

        {/* <div className={styles.footer_copyright}>
        <span className={styles.footer_copyright_text}>
        Copyright ©2024 All rights reserved | This template is made with  by Colorlib
        </span>
      </div> */}
      </div>
    </div>
  );
};

export default Footer;
