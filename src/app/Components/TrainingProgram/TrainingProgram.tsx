import React from 'react';
import styles from './TrainingProgram.module.css';
import { TRAINING_PROGRAM_DATA } from '@/constanst/constanst';

const TrainingProgram = () => {
  return (
    <div className={styles.training_program}>
      <h2 className={styles.training_program_title}>Our free training program</h2>
      <ul className={styles.training_program_list}>
        {TRAINING_PROGRAM_DATA.map((item, index) => (
          <li key={index} className={styles.training_program_item}>
            <h3 className={styles.training_program_item_title}>{item.title}</h3>
            <div className={styles.training_program_item_desc}>
              <ul>
                {item.description.map((desc, descIndex) => (
                  <li key={descIndex}>{desc}</li>
                ))}
              </ul>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TrainingProgram;