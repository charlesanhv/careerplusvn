export function generateName() {
  const firstNames = [
    "Charles",
    "Anh",
    "David",
    "Emily",
    "John",
    "Jane",
    "Michael",
    "Sarah",
    "Robert",
    "Jessica",
    "William",
    "Olivia",
    "Thomas",
    "Emma",
    "James",
    "Sophia",
    "Daniel",
    "Ava",
    "Matthew",
  ]; // Thêm các tên bạn muốn vào đây
  const lastNames = [
    "Nguyen",
    "Tran",
    "Le",
    "Pham",
    "Hoang",
    "Phan",
    "Vu",
    "Dang",
    "Bui",
    "Do",
    "Robeton",
    "Downy",
    "Nathan",
    "Neil",
    "Austin",
  ]; // Thêm các họ bạn muốn vào đây

  const firstName = firstNames[Math.floor(Math.random() * firstNames.length)];
  const lastName = lastNames[Math.floor(Math.random() * lastNames.length)];

  return `${firstName} ${lastName}`;
}
