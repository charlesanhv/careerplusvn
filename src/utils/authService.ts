// utils/authService.js

import { logError } from ".";

/**
 * Sets the authentication token in localStorage.
 * @param {string} token - The token to store.
 */
export function setAuthToken(token: string) {
    localStorage.setItem('AUTH_TOKEN', token);
}

/**
 * Retrieves the authentication token from localStorage.
 * @returns {string|null} The retrieved token or null if not found.
 */
export function getAuthToken() {
    return localStorage.getItem('AUTH_TOKEN');
}

/**
 * Clears the authentication token from localStorage.
 */
export function clearAuthToken() {
    localStorage.removeItem('AUTH_TOKEN');
}

/**
 * Refreshes the authentication token by calling the API and updating the stored token.
 * @returns {Promise<string|null>} The new token if the refresh is successful, null otherwise.
 */
export async function refreshToken() {
    try {
        const response = await fetch('http://api.yourdomain.com/auth/refresh', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getAuthToken()}`
            },
        });

        if (!response.ok) {
            logError("Failed to refresh token:", response.statusText);
            clearAuthToken(); // Clear the possibly expired token
            return null;
        }

        const { accessToken } = await response.json();
        setAuthToken(accessToken); // Save the new token
        return accessToken;
    } catch (error) {
        logError("Error during token refresh:", error);
        clearAuthToken();
        return null;
    }
}
