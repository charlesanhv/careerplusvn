/* eslint-disable @typescript-eslint/ban-ts-comment */
import clsx from "clsx"
import styles from "./Heading.module.css"

// @ts-ignore
// eslint-disable-next-line react/prop-types
const Heading = ({title,description}) => {
    return (
        <div className={styles.heading}>
            <div className={clsx(styles.heading_content,'container')}>
                <div className="row" >
                    <div className={styles.heading_content_wrapper}>
                <div className={styles.heading_title}>{title}</div>
                <div className={styles.heading_description}>{description}</div>
                </div>
                </div>
            </div>
        </div>
    )
}

export default Heading