import { generateName } from "@/utils";
import { v4 as uuidv4, v4 } from "uuid";
export const PHONE_CALL_US = "(+84) 962501336";
export const TOP_STORIES = [
  {
    img: "/images/our-team/gam.jpg",
    name: "Gam Tran",
    occupation: "Director",
    desc: [
      {
        id: uuidv4(),
        title: "University of Commerce",
      },
      {
        id: uuidv4(),
        title:
          "Master of Human Resources Management in University of Western Australia",
      },
      {
        id: uuidv4(),
        title: "9 years experiences in Teacher Recruitment and Management",
      },
    ],
  },
  {
    img: "/images/our-team/lua.webp",
    name: "Lua Tran",
    occupation: "Sales Manager",
    desc: [
      {
        id: uuidv4(),
        title: "National Economic University",
      },
      {
        id: uuidv4(),
        title: "CSL certification",
      },
      {
        id: uuidv4(),
        title: "3 years of being sale manager in different industries",
      },
    ],
  },
  {
    img: "/images/our-team/khoa.jpg",
    name: "Khoa Tran",
    occupation: "Legal Manager",
    desc: [
      {
        id: uuidv4(),
        title: "Hanoi Law University",
      },
      {
        id: uuidv4(),
        title: "CP certification",
      },
      {
        id: uuidv4(),
        title: "5 years working in legal field",
      },
    ],
  },
  {
    img: "/images/our-team/naomi.jpg",
    name: "Naomi White",
    occupation: "HR Executive",
    desc: [
      {
        id: uuidv4(),
        title: "UNISA University",
      },
      {
        id: uuidv4(),
        title: "CELTA and Grapeseed certificate",
      },
      {
        id: uuidv4(),
        title: "IELTS 8.5",
      },
      {
        id: uuidv4(),
        title: "2 years experiences teaching in China",
      },
      {
        id: uuidv4(),
        title: "4 years experiences teaching in Viet Nam",
      },
    ],
  },
  {
    img: "/images/our-team/angelo.png",
    name: "Angelo Barnard",
    occupation: "HR Executive",
    desc: [
      {
        id: uuidv4(),
        title: "Cape Penisula University of Technology",
      },
      {
        id: uuidv4(),
        title: "C1 Certificate",
      },
      {
        id: uuidv4(),
        title: "Academic Manager in TH Hub",
      },
      {
        id: uuidv4(),
        title: "3 years in performance management",
      },
    ],
  },
  {
    img: "/images/our-team/damon.jpg",
    name: "Damon",
    occupation: "HR Executive",
    desc: [
      {
        id: uuidv4(),
        title: "4 years of Recruitment and Talent Acquisition",
      },
      {
        id: uuidv4(),
        title: "NewCastle University",
      },
      {
        id: uuidv4(),
        title: "CELTA Certificate",
      },
      {
        id: uuidv4(),
        title: "Manager of HR Department",
      },
    ],
  },
];

export const PROGRAMS = {
  title: "JOB OPPORTUNITIES WE OFFER",
  description:
    "We have partnerships with several organizations across Vietnam. You have the option to select from offline positions at schools or language centers, as well as online teaching roles that can be conducted remotely from any location.",
  data: [
    {
      img: "/images/falculty_of_science.webp",
      title: "Teaching position at schools",
      des: "The teachers will be employed at public or private schools across Vietnam. Working hours predominantly occur on weekdays.",
      slug: "teaching-position-at-schools",
    },
    {
      img: "/images/falculty_of_art.webp",
      title: "Teaching position at language centers",
      des: "Unlike teaching roles within schools, educators have the option to work at language centers located in Hanoi, Ho Chi Minh City, or various provinces throughout Vietnam.",
      slug: "teaching-position-at-language-centers",
    },
    {
      img: "/images/jobs/teaching.webp",
      title: "Online teaching",
      des: "",
      slug: "online-teaching",
    },
  ],
};

export const PROGRAMS_FULL = [
  {
    img: "/images/falculty_of_science.webp",
    title: "Falculty of Science",
    des: "Integer efficitur tellus metus, sed feugiat leo posuere ac. Morbi vitae tincidunt mi, et malesuada massa. Sed blandit placerat elit sit amet condimentum.",
  },
  {
    img: "/images/falculty_of_art.webp",
    title: "Falculty of Art",
    des: "Integer efficitur tellus metus, sed feugiat leo posuere ac. Morbi vitae tincidunt mi, et malesuada massa. Sed blandit placerat elit sit amet condimentum.",
  },
  {
    img: "/images/falculty_of_science_2.webp",
    title: "Falculty of Science",
    des: "Integer efficitur tellus metus, sed feugiat leo posuere ac. Morbi vitae tincidunt mi, et malesuada massa. Sed blandit placerat elit sit amet condimentum.",
  },
  {
    img: "/images/falculty_of_art_2.webp",
    title: "Falculty of Art",
    des: "Integer efficitur tellus metus, sed feugiat leo posuere ac. Morbi vitae tincidunt mi, et malesuada massa. Sed blandit placerat elit sit amet condimentum.",
  },
];

export const ESSENTIAL_RESOURCES = {
  title: "LIVING IN VIETNAM",
  description: "",
  content: [
    {
      key: "first_year_students",
      title: "VIETNAMESE CULTURE",
      desc: `Explore Vietnam's rich cultural heritage, featuring numerous renowned landmarks and a diverse array of cuisine.`,
    },
    {
      key: "tuition_and_fees",
      title: "ACTIVITIES",
      desc: "Explore essential tourism activities and experiences that are not to be missed.",
    },
    {
      key: "international_students",
      title: "LIVING COSTS",
      desc: "Gain insights into the cost of living in Vietnam to facilitate straightforward financial management.",
    },
  ],
};

export const BLOGS = [
  {
    id: v4(),
    image: "/images/why-us/04.jpg",
    title: "Access to various teaching opportunities ",
    desc: `At Grow Green Vietnam, we offer a wide range of job opportunities at our own language center in Hanoi, as well as through partnerships with other language centers, public schools, and international schools in Hanoi and nearby cities. This variety provides candidates with numerous options to find the perfect teaching position that aligns with their preferences and career goals.`,
  },
  {
    id: v4(),
    image: "/images/why-us/05.jpg",
    title: "Free Training Course",
    desc: `We believe in setting our teachers up for success. That's why we offer a comprehensive two-week training course, valued at $700, completely free of charge. This includes two weeks of free accommodation in Hanoi, intensive theoretical training, observation sessions with our top teachers, and hands-on teaching experience. This program is designed to help even those without prior teaching experience discover their potential and grow in confidence.`,
  },
  {
    id: v4(),
    image: "/images/why-us/03.jpg",
    title: "Free Visa Guidance",
    desc: `Navigating visa requirements can be challenging, but our professional legal team is well-versed in Vietnamese law and will guide you through the entire process. We handle the complexities, so you can focus on what matters most—teaching and exploring your new surroundings.`,
  },
  {
    id: v4(),
    image: "/images/why-us/02.webp",
    title: "Experienced Support Team",
    desc: `We understand that moving to a new country can be daunting. That's why we have an experienced support team dedicated to helping you settle into life in Vietnam. From finding accommodation in Hanoi to navigating transportation and managing daily essentials, our team is here to ensure your transition is as smooth as possible.`,
  },
];

export const BlOG_CATAGORIES = [
  {
    name: "Resaurant food",
    number: 37,
  },
  {
    name: "Travel news",
    number: 10,
  },
  {
    name: "Modern technology",
    number: 3,
  },
  {
    name: "Product",
    number: 11,
  },
  {
    name: "Inspiration",
    number: 21,
  },
  {
    name: "Health Care",
    number: 9,
  },
];

export const RECENT_POST = [
  {
    image: "/images/recent_post_1.webp",
    title: "From life was you fish...",
    time: "January 12, 2019",
  },
  {
    image: "/images/recent_post_2.webp",
    title: "The Amazing Hubble",
    time: "02 Hours ago",
  },
  {
    image: "/images/recent_post_3.webp",
    title: "Astronomy Or Astrology",
    time: "03 Hours ago",
  },
  {
    image: "/images/recent_post_4.webp",
    title: "Asteroids telescope",
    time: "01 Hours ago",
  },
];

export const TAG_CLOUD = [
  "grow green",
  "love",
  "education",
  "travel",
  "restaurant",
  "life style",
];

export const Instagram = [
  {
    image: "/images/post_5.webp",
  },
  {
    image: "/images/post_6.webp",
  },
  {
    image: "/images/post_7.webp",
  },
  {
    image: "/images/post_8.webp",
  },
  {
    image: "/images/post_9.webp",
  },
  {
    image: "/images/post_10.webp",
  },
];

export const BLOG_DETAILS = {
  image: "/images/blog_1.webp",
  title:
    "Second divided from form fish beast made every of seas all gathered us saying he our",
  author: "Travel, Lifestyle",
  number_cmt: 3,
  desc_1:
    "MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower",
  desc_2:
    "MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually",
  desc_3:
    "MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.",
  desc_4:
    "MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower",
  desc_5:
    "MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower",
};

export const COMMENTS = [
  {
    avatar: `https://i.pravatar.cc/150?img=${Math.floor(Math.random() * 70)}`, // T?o URL hình ảnh avatar ng?u nh?n online v? ng?u nh?n
    name: generateName(),
    time: new Date(
      Date.now() - Math.floor(Math.random() * 1000 * 60 * 60 * 24 * 365 * 2)
    ).toLocaleString("en-US", {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    }),
    content:
      "This is a fantastic article! I'm considering teaching in Vietnam and this has provided me with valuable insights into the cultural richness and professional opportunities available.",
  },
  {
    avatar: `https://i.pravatar.cc/150?img=${Math.floor(Math.random() * 70)}`, // T?o URL hình ảnh avatar ng?u nh?n online v? ng?u nh?n
    name: generateName(), // Random name from a to z with length from 8 to 15
    time: new Date(
      Date.now() - Math.floor(Math.random() * 1000 * 60 * 60 * 24 * 365 * 2)
    ).toLocaleString("en-US", {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    }),
    content:
      "I agree, the affordability of living in Vietnam is a major draw. It's great to hear about the respect for educators and the supportive teaching environment.",
  },
  {
    avatar: `https://i.pravatar.cc/150?img=${Math.floor(Math.random() * 70)}`, // T?o URL hình ảnh avatar ng?u nh?n online v? ng?u nh?n
    name: generateName(),
    time: new Date(
      Date.now() - Math.floor(Math.random() * 1000 * 60 * 60 * 24 * 365 * 2)
    ).toLocaleString("en-US", {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    }),
    content:
      "I'm particularly interested in the natural beauty and adventure opportunities that Vietnam offers. It sounds like a great place to explore during my free time.",
  },
  // {
  //   avatar: `https://i.pravatar.cc/150?img=${Math.floor(Math.random() * 70)}`, // T?o URL hình ảnh avatar ng?u nh?n online v? ng?u nh?n
  //   name: "Sarah Brown",
  //   time: new Date(
  //     Date.now() - Math.floor(Math.random() * 1000 * 60 * 60 * 24 * 365 * 2)
  //   ).toLocaleString("en-US", {
  //     year: "numeric",
  //     month: "long",
  //     day: "numeric",
  //     hour: "2-digit",
  //     minute: "2-digit",
  //   }),
  //   content:
  //     "The food in Vietnam is amazing! I can't wait to try all the delicious dishes mentioned in the article.",
  // },
  // {
  //   avatar: `https://i.pravatar.cc/150?img=${Math.floor(Math.random() * 70)}`, // T?o URL hình ảnh avatar ng?u nh?n online v? ng?u nh?n
  //   name: "Michael Kim",
  //   time: new Date(
  //     Date.now() - Math.floor(Math.random() * 1000 * 60 * 60 * 24 * 365 * 2)
  //   ).toLocaleString("en-US", {
  //     year: "numeric",
  //     month: "long",
  //     day: "numeric",
  //     hour: "2-digit",
  //     minute: "2-digit",
  //   }),
  //   content:
  //     "Thank you for this comprehensive guide. It's really helpful for anyone thinking about teaching in Vietnam.",
  // },
];

export const TRAINING_PROGRAM_DATA = [
  {
    title: "2 Days of Theory Training",
    description: [
      "Introduction to Vietnam and the teaching market.",
      "Differences between teaching in schools and language centers.",
      "Lesson planning techniques.",
      "Classroom activities, including resources for games and songs.",
      "Classroom management",
      "Marketing tasks at schools.",
      "Methods for student assessment and more.",
    ],
  },
  {
    title: "3 Days of Observation",
    description: [
      "Watch our experienced teachers in action.",
      "Understand real classroom dynamics and effective teaching practices.",
    ],
  },
  {
    title: "2 Days of Practical Teaching",
    description: [
      "Co-teach alongside other teachers.",
      "Gradually build your teaching skills and familiarity with classroom management.",
    ],
  },
  {
    title: "Remaining Time",
    description: [
      "Teach in class with ongoing observation from teachers and the HR manager.",
      "Receive constructive feedback to improve your teaching methods and ensure success.",
    ],
  },
];

export const dataBogDetails = [
  {
    id: v4(),
    title: "Reasons for Choosing Vietnam as Your Destination",
    slug: "reasons-choosing-vietnam-as-your-destination",
    sections: [
      {
        heading: "Introduction",
        content:
          "Opting for Vietnam as your teaching destination presents numerous professional and personal advantages, establishing it as an outstanding place to live and work. Here are the key reasons why Vietnam is a distinguished choice:",
      },
      {
        heading: "Cultural Richness",
        content:
          "Vietnam boasts a vibrant culture, extensive history, and diverse traditions, providing a unique environment for educators. Whether you are in the bustling city of Hanoi or exploring the tranquil countryside, you will encounter a culture that is both dynamic and welcoming.",
      },
      {
        heading: "Respect for Education",
        content:
          "Education holds significant value in Vietnam, and teachers are accorded considerable respect. This esteem fosters a supportive and rewarding teaching environment, where your contributions are acknowledged and where you can make a substantial impact on eager students.",
      },
      {
        heading: "Affordable Living",
        content:
          "The cost of living in Vietnam is notably affordable, allowing you to enjoy a high standard of living while saving money. The costs associated with housing, food, and entertainment are reasonable, facilitating a comfortable and secure lifestyle.",
      },
      {
        heading: "Professional Opportunities",
        content:
          "Vietnam’s expanding education sector offers a wealth of career advancement opportunities. Whether you seek to gain classroom experience, develop new skills, or pursue leadership roles, Vietnam provides a dynamic setting conducive to professional growth.",
      },
      {
        heading: "Natural Beauty and Adventure",
        content:
          "Vietnam is renowned for its stunning landscapes and abundant adventure opportunities. From the picturesque Ha Long Bay to the historic town of Hoi An, there are numerous destinations to explore and experiences to enjoy beyond your professional duties.",
      },
      {
        heading: "Delicious Cuisine",
        content:
          "Vietnam is celebrated for its fresh and flavorful cuisine. Residing here affords you daily access to a variety of delectable dishes, making each meal an exciting culinary experience.",
      },
      {
        heading: "Conclusion",
        content:
          "In conclusion, Vietnam represents an ideal destination for educators seeking a blend of professional development, cultural immersion, and personal adventure. Its rich cultural heritage, affordable living costs, and hospitable environment create a setting where you can excel both within and beyond the classroom.",
      },
    ],
    listImages: [
      {
        id: v4(),
        url: "/images/blog/b1/01.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/02.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/03.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/04.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/05.jpg",
      },
    ],
  },
  {
    id: v4(),
    title: "Guidelines for Staying in Vietnam",
    slug: "staying-in-vietnam",
    sections: [
      {
        heading: "What to Pack",
        introduction:
          "When preparing for a teaching assignment in Vietnam, it is important to pack thoughtfully to ensure you have all necessary items. The following guide will assist you in this preparation:",
        categories: [
          {
            name: "Essentials",
            items: [
              {
                name: "Passport and Visa",
                description:
                  "Ensure your passport remains valid for at least six months. Bring all requisite visa documents, including a printed approval letter and two passport-sized photos.",
              },
              {
                name: "Teaching Documents",
                description:
                  "Pack essential documents such as your teaching certificates, degree, letters of reference, and any other pertinent paperwork. It is advisable to have both physical and digital copies available.",
              },
            ],
          },
          {
            name: "Clothing",
            items: [
              {
                name: "Lightweight Clothing",
                description:
                  "Given Vietnam’s typically hot and humid climate, pack light, breathable clothing such as cotton shirts, dresses, and shorts.",
              },
              {
                name: "Winter Clothing",
                description:
                  "For the cooler months, especially in northern regions, include warmer garments such as sweaters, jackets, and long pants.",
              },
              {
                name: "Professional Attire",
                description:
                  "Schools and language centers generally require business-casual attire. Pack professional outfits, including collared shirts, blouses, and modest skirts or trousers.",
              },
              {
                name: "Size Considerations",
                description:
                  "If you wear larger sizes, bring an adequate supply of clothing, as it may be challenging to find suitable sizes locally.",
              },
            ],
          },
          {
            name: "Technology",
            items: [
              {
                name: "Laptop and Accessories",
                description:
                  "A laptop is crucial for teaching and lesson preparation. Bring necessary chargers, a portable power bank, and multiple adapters, as Vietnam operates on a 220V system with Type A or C plugs.",
              },
              {
                name: "Unlocked Phone",
                description:
                  "Consider bringing an unlocked smartphone to facilitate the purchase of a local SIM card.",
              },
            ],
          },
        ],
        conclusion:
          "This list is intended to help you pack efficiently and ensure that you are well-prepared for your teaching experience in Vietnam.",
      },
    ],
    listImages: [
      {
        id: v4(),
        url: "/images/blog/b1/02.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/03.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/04.jpg",
      },
      {
        id: v4(),
        url: "/images/blog/b1/05.jpg",
      },
    ],
  },
];
