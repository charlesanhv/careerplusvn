"use client";

import style from "./header.module.css";
import Image from "next/image";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSquareFacebook,
  faWhatsapp,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { motion } from "framer-motion";
import clsx from "clsx";
import { PHONE_CALL_US } from "@/constanst/constanst";

const items = [
  {
    key: "",
    label: "Home",
  },
  {
    key: "about",
    label: "About",
  },
  {
    key: "jobs",
    label: "Job Offers",
  },
  {
    key: "why-us",
    label: "Why us",
  },
  {
    key: "blog",
    label: "Blog",
  },
  {
    key: "contact",
    label: "Contact",
  },
];


const SOCIAL_ICON = [
  {
    name: "whatsapp",
    icon: faWhatsapp,
    color: "#3B5998",
    link: "+84962501336",
  },
  {
    name: "facebook",
    icon: faSquareFacebook,
    color: "#3B5998",
    link: "https://www.facebook.com/anhngu.2g",
  },
  // {
  //   name: 'twitter',
  //   icon: faSquareTwitter,
  //   color: '#1DA1F2',
  //   link: 'https://twitter.com/'
  // },
  // {
  //   name: 'youtube',
  //   icon: faSquareYoutube,
  //   color: '#FF0000',
  // },
  {
    name: "email",
    icon: faEnvelope,
    color: "#007A5C",
    link: "tuyendung@growgreenvietnam.com",
  },
];

const BLOG = [
  { name: "Blog", key: "blogs" },
  { name: "Blog Details", key: "blog_details" },
  // { name: "Elements", key: "elements" },
];

const HeaderCollapse: React.FC<any> = ({
  showMenuMobile,
  onCLickMenu,
  showBLogOption,
  onCLickBlog,
}: any) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleOpenModal = (label: string) => {
    label === "Blogs" && setIsOpen(true);
  };
  const handleCloseModal = (label: string) => {
    label === "Blogs" && setIsOpen(false);
  };

  const handleClickSocial = (item: any) => {
    switch (item.name) {
      case "whatsapp":
        window.open(`https://wa.me/${item.link}`, "_blank");
        break;
      case "facebook":
        window.open(`https://m.me/${item.link.split("/").pop()}`, "_blank");
        break;
      case "email":
        window.open(`mailto:${item.link}`, "_blank");
        break;
      default:
        break;
    }
  };


  return (
    <motion.div
      className={style.header_area}
      id="Header"
      initial={{ opacity: 0, y: -50 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.3 }}
    >
      <div className="container">
        <div className="centerItemBox" style={{ padding: "15px 20px" }}>
          <div className={style.header_bottom_container}>
            <div className="centerItemBox header_left">
              <div className={style.wrapper}>
                <a href="/" className={style.wrapper_logo}>
                  <Image
                    className={style.logo}
                    src="/images/growgreen-logo.png"
                    alt="logo"
                    width={171}
                    height={49}
                  />
                  <div className={style.wrapper_logo_text}>
                  <span>GROW GREEN</span>
                      <span>VIETNAM</span>
                  </div>
                </a>
              </div>
              <div className={style.menu_container}>
                {items.map((item, index) => {
                  return (
                    <a
                      onMouseEnter={() => handleOpenModal(item?.label)}
                      onMouseLeave={() => handleCloseModal(item?.label)}
                      key={index}
                      href={`/${item?.key}`}
                      className={clsx(style.menu_item, {
                        [style.blog_link]: item?.label === "Blogs",
                      })}
                      style={
                        item?.label === "Blogs" ? { position: "relative" } : {}
                      }
                    >
                      {item?.label}

                      {item?.label === "Blogs" && isOpen && (
                        <motion.div
                          className={style.blog_option_wrapper}
                          initial={{ opacity: 0, y: 20 }}
                          animate={{ opacity: 1, y: 0 }}
                          transition={{ duration: 0.3 }}
                        >
                          {BLOG.map((item, index) => {
                            return (
                              <a
                                key={index}
                                className={style.blog_option}
                                href={`/${item?.key}`}
                              >
                                <span className={style.blog_option_text}>
                                  {item.name}
                                </span>
                              </a>
                            );
                          })}
                        </motion.div>
                      )}
                    </a>
                  );
                })}
              </div>
            </div>

            <div className="centerItemBox header_right">
              <div className={style.header_right_container}>
                <div className={style.call_us}>
                  Call Us :{" "}
                  <span className={style.phone_number}>{PHONE_CALL_US}</span>
                </div>

                <div className={style.social_wrapper}>
                  {/* <i className="fa fa-facebook-square" aria-hidden="true"></i> */}
                  {SOCIAL_ICON.map((item, index) => {
                    return (
                      <span key={index} className={style.social_icon_wrapper}   onClick={() => handleClickSocial(item)}>
                        <FontAwesomeIcon
                          icon={item.icon}
                          size="xl"
                          color={item.color}
                        />
                      </span>
                    );
                  })}
                </div>

                <div className="menu_mobile" onClick={onCLickMenu}>
                  <span className="menu_mobile_slickNav" />
                  <span className="menu_mobile_slickNav" />
                  <span className="menu_mobile_slickNav" />
                </div>
              </div>
            </div>

            <div className="menu_mobile_wrapper">
              {showMenuMobile && (
                <motion.div
                  initial={{ opacity: 0, height: 0 }}
                  animate={{ opacity: 1, height: "auto" }}
                  exit={{ height: 0 }}
                  transition={{ duration: 0.3 }}
                  className="header_menu_mobile"
                >
                  {items.map((item, index) => {
                    return item.key !== "blogs" ? (
                      <a
                        key={index}
                        href={`/${item?.key}`}
                        className="header_menu_mobile_item"
                      >
                        {item?.label}
                      </a>
                    ) : (
                      <>
                        <a
                          key={index}
                          // href={`/${item?.key}`}
                          className="header_menu_mobile_item"
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                          onClick={onCLickBlog}
                        >
                          <span className="header_menu_mobile_item_text">
                            {" "}
                            {item?.label}
                          </span>
                          <span
                            className="header_menu_mobile_item_text"
                            style={{ fontSize: "22px" }}
                          >
                            +
                          </span>
                        </a>
                        {showBLogOption &&
                          BLOG.map((item, index) => {
                            return (
                              <motion.a
                                key={index}
                                href={`/${item?.key}`}
                                className="header_menu_mobile_item"
                                style={{ paddingLeft: "60px" }}
                                initial={{ opacity: 0, height: 0 }}
                                animate={{ opacity: 1, height: "auto" }}
                                exit={{ opacity: 0, height: 0 }}
                                transition={{ duration: 0.3 }}
                              >
                                {item.name}
                              </motion.a>
                            );
                          })}
                      </>
                    );
                  })}
                </motion.div>
              )}
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default HeaderCollapse;
