'use client';

import styles from './styles.module.css';
import { clsx } from 'clsx';
import TeachingApproach from './Components/TeachingApproach/TeachingApproach';
import EssentialResources from './Components/EssentialResources/EssentialResources';
import OurPartners from './Components/OurPartners/OurPartners';
import { motion } from 'framer-motion';
import ProgramOffer from './Components/ProgramOffer/ProgramOffer';
import Link from 'next/link';

export default function Home() {
  //
  // const PROGRAMS = [
  //   {
  //     img: "/images/falculty_of_science.webp",
  //     title: "Falculty of Science",
  //     des: "Integer efficitur tellus metus, sed feugiat leo posuere ac. Morbi vitae tincidunt mi, et malesuada massa. Sed blandit placerat elit sit amet condimentum.",
  //   },
  //   {
  //     img: "/images/falculty_of_art.webp",
  //     title: "Falculty of Art",
  //     des: "Integer efficitur tellus metus, sed feugiat leo posuere ac. Morbi vitae tincidunt mi, et malesuada massa. Sed blandit placerat elit sit amet condimentum.",
  //   },
  // ];

  return (
    <div className={styles.homepage}>
      <div className={styles.heading}>
        <div className="container">
          <div
            className={styles.content_wrapper}
            style={{ alignItems: 'start' }}
          >
            <div className={styles.content}>
              {/* <motion.span
                className={styles.heading_caption}
                initial={{ opacity: 0, y: 20 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.3 }}
              >
                EDUCATION & SCHOOL
              </motion.span> */}
              <motion.h1
                className={clsx(
                  styles.heading_caption,
                  styles.heading_caption_2
                )}
                initial={{ opacity: 0, y: 50 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.17, delay: 0.2 }}
              >
                SHOWCASE JOBS, EVENTS AND MORE!
              </motion.h1>
              {/* <motion.span
                className={clsx(
                  styles.heading_caption,
                  styles.heading_caption_3
                )}
                initial={{ opacity: 0, y: 120 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.37, delay: 0.5 }}
              >
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse varius enim in eros elementum tristique.
              </motion.span> */}
              <Link
                href="/jobs"
                // initial={{ opacity: 0, y: 100 }}
                // animate={{ opacity: 1, y: 0 }}
                // transition={{ duration: 0.57, delay: 0.7 }}
              >
                <div className={styles.heading_button}>Get Started Now</div>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <TeachingApproach />

      <ProgramOffer />

      <EssentialResources />
      {/* <TopStories /> */}
      <OurPartners />
    </div>
  );
}
