import styles from "./styles.module.css";
// import Heading from "../Components/Heading/Heading";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faComment, faUser } from "@fortawesome/free-solid-svg-icons";

import Image from "next/image";
import BlogSidebar from "../Components/BlogSidebar/BlogSidebar";
import { dataBogDetails } from "@/constanst/constanst";
import Link from "next/link";
// const NUMPAGE = [1, 2, 3, 4];

export const metadata = {
  title: 'Grow Green Blog',
  openGraph: {
    title: 'Grow Green Blog',
  },
}

const Blog = async () => {
  return (
    <div className={styles.blog_container}>
      {/* <Heading
        title="Blog"
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique."
      /> */}

      <div className={styles.blogs_wrapper}>
        <div className={styles.blogs}>
          {dataBogDetails.map((item: any, index) => {
            // Lấy URL hình ảnh đầu tiên từ listImages
            const randomIndex = Math.floor(Math.random() * item.listImages.length);
            const imageUrl = item.listImages?.[randomIndex]?.url || "/images/placeholder.jpg"; 
            return (
              <div className={styles.blog_item} key={index}>
                <div className={styles.blog_img_wrapper}>
                  <Image
                    src={imageUrl}
                    className={styles.blog_img}
                    alt="image"
                    width={500}
                    height={500}
                  />
                  <div className={styles.blog_date}>
                    <span className={styles.blog_date_text}>
                      {new Date().getDate()}
                    </span>
                    <span className={styles.blog_month_text}>
                      {new Date(Date.now() - 1000 * 60 * 60 * 24 * 30).toLocaleString("default", { month: "long" })}
                    </span>
                  </div>
                </div>
                <div className={styles.blog_content_wrapper}>
                  <Link href={`/blog/${item.slug}`} className={styles.blog_title}>{item.title}</Link>
                  {/* Hiển thị phần giới thiệu hoặc nội dung đầu tiên */}
                  <div className={styles.blog_description}>
                    {item.introduction || item.sections?.[0]?.content} 
                  </div>
                  <div className={styles.blog_info_link}>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        gap: "10px",
                      }}
                    >
                      <FontAwesomeIcon icon={faUser} color="#999999" />
                      {/* Bạn có thể thay thế tác giả bằng dữ liệu động nếu có */}
                      <span className={styles.link_text}>Admin</span> 
                    </div>

                    <div className={styles.divider} />

                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        gap: "10px",
                      }}
                    >
                      <FontAwesomeIcon icon={faComment} color="#999999" />
                      {/* Bạn có thể thay thế số lượng bình luận bằng dữ liệu động nếu có */}
                      <span className={styles.link_text}>03 Comments</span> 
                    </div>
                  </div>
                </div>
              </div>
            );
          })}

          {/* <div className={styles.blog_pagination}>
            <div className={styles.page_item_wrapper}>
              <a className={styles.page_item}>{`<`}</a>
            </div>
            {NUMPAGE.map((item, index) => {
              return (
                <div className={styles.page_item_wrapper} key={index}>
                  <a className={styles.page_item}>{item}</a>
                </div>
              );
            })}
            <div className={styles.page_item_wrapper}>
              <a className={styles.page_item}>{`>`}</a>
            </div>
          </div> */}
        </div>

        <BlogSidebar />
      </div>
    </div>
  );
};

export default Blog;