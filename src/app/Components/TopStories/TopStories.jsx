import clsx from "clsx";
import styles from "./styles.module.css";
import { TOP_STORIES } from "@/constanst/constanst";
import Image from "next/image";
const TopStories = ({ type = "home" }) => {
  return (
    <div className={clsx(styles.content_wrapper, styles.top_stories)}>
      <div className={styles.top_stories_wrapper}>
        <h2 className={clsx(styles.content_title, "title")}>
          {type === "home" ? "Our partners" : "Our team"}{" "}
        </h2>
        <div
          className={clsx(styles.content_description, styles.partner_des)}
          style={{ marginBottom: "40px" }}
        ></div>
        <div className={styles.top_stories_content}>
          {TOP_STORIES.map((item, index) => (
            <div className={styles.top_stories_item} key={index}>
              <Image
                unoptimized
                className={styles.top_stories_item_img}
                src={item.img}
                alt="image"
                width={200}
                height={450}
              />
              <div className={styles.top_stories_item_content}>
                <h3 className={styles.top_stories_item_title}>{item.name}</h3>
                <span className={styles.top_stories_item_occupation}>
                  {item.occupation}
                </span>
                <div className={styles.top_stories_item_description}>
                  <ul>
                    {item?.desc.map((item, index) => (
                      <li key={index}>{item.title}</li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default TopStories;
