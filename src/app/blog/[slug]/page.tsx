import BlogDetail from "@/app/Components/BlogDetail/BlogDetail";
import { dataBogDetails } from "@/constanst/constanst";


export const generateMetadata = ({ params }: { params: { slug: string } }) => {
  const dataDetail: any = dataBogDetails.find((job) => job.slug === params.slug);

  if (!dataDetail) {
    return {
      title: "Job not found",
      description: "The requested job could not be found.",
    };
  }

  return {
    title: dataDetail.title,
    description: dataDetail.sections[0].content || dataDetail.sections[0].introduction || "",
  };
};

export default function BlogDetailPage({
  params,
}: {
  params: { slug: string };
}) {
  return <BlogDetail slug={params.slug} />;
}
