import ProgramOffer from "../Components/ProgramOffer/ProgramOffer"

export const metadata = {
    title: 'Grow Green Jobs - Grow Green Vietnam',
    openGraph: {
      title: 'Grow Green Jobs - Grow Green Vietnam',
    },
  }
const JobOffers = async () => {
    return (
       <>
       {/* <Heading title="JobOffers" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique." /> */}
       <ProgramOffer />
       {/* <OurPartners /> */}
       </>
    )
}

export default JobOffers