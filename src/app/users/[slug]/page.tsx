"use client";
import { useEffect, useState } from "react";

type DetailUserPageProps = {
  params: { slug: string };
};

const DetailUserPage = ({ params }: DetailUserPageProps) => {
  const [user, setUser] = useState<any>(null);

  useEffect(() => {
    const fetchUser = async () => {
      const response = await fetch(`/api/users/${params.slug}`);
      const data = await response.json();
      setUser(data);
    };

    fetchUser();
  }, [params.slug]);

  return (
    <div>
      <h1>{params?.slug}</h1>
      {user && (
        <>
          <h1>{params.slug}</h1>
        </>
      )}
    </div>
  );
};

export default DetailUserPage;
