export type PageProps<P = object> = {
  params?: P;
};

export interface IUsersData {
  id: string;
  name: string;
  email: string;
}

export interface IMetaUsers {
  current: number;
  pageSize: number;
  total: number;
}

interface UserData {
  id?: string;
  [key: string]: any; // Still allows for flexible properties but with a more descriptive structure
}

interface FetchAPIOptions {
  tags?: string[];
  revalidate?: number;
}