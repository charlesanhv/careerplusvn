// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
"use client";

import clsx from "clsx";
import styles from "./styles.module.css";
import { ESSENTIAL_RESOURCES } from "@/constanst/constanst";
import { useState } from "react";
import Image from "next/image";

const EssentialResources = () => {
  const [changeColor, setChangeColor] = useState({
    first_year_students: false,
    tuition_and_fees: false,
    international_students: false,
  });
  return (
    <div className={clsx(styles.content_wrapper, styles.essential_resources)}>
      <div className={styles.essential_resources_main}>
        <span className={clsx(styles.content_title,'title')}>{ESSENTIAL_RESOURCES?.title}</span>
        <span
          className={clsx(styles.content_description, styles.program_des)}
          style={{ marginBottom: "40px" }}
        >
          {ESSENTIAL_RESOURCES?.description}
        </span>
        {ESSENTIAL_RESOURCES?.content?.map((item) => (
          <div
            className={styles.essential_resources_item}
            key={item.key}
            onMouseEnter={() =>
              setChangeColor({ ...changeColor, [item.key]: true })
            }
            onMouseLeave={() =>
              setChangeColor({ ...changeColor, [item.key]: false })
            }
          >
            <div
              className={styles.essential_resources_item_text_wrapper}
              style={{
                display: "flex",
                flexDirection: "column",
                gap: "13px",
              }}
            >
              <span className={styles.essential_resources_item_title}>
                {item.title}
              </span>
              <span
                className={clsx(
                  styles.content_description,
                  styles.essential_resources_item_description
                )}
              >
                {item.desc}
              </span>
            </div>

            {/* <a className={styles.essential_resources_item_link}>
              <FontAwesomeIcon
                icon={faAngleRight}
                className={styles.arrow_icon}
                // color={changeColor[item.key] ? "#fff" : "#007A5C"}
                size="xl"
              />
            </a> */}
          </div>
        ))}
      </div>

      <Image
        className={styles.essential_resources_img}
        src="/images/essential_resources.webp"
        alt="image"
        width={500}
        height={500}
      />
    </div>
  );
};

export default EssentialResources;
