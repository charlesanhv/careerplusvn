"use client";
import { usePathname, useRouter } from "next/navigation";
import { Space, Table } from "antd";
import { ColumnType } from "antd/es/table";
import { Button, Popconfirm } from "antd";
import { IMetaUsers, IUsersData } from "@/types";
import { useEffect, useState } from "react";
import CreateUser from "./create.user";
import { handleDeleteUserAction } from "@/action";
import UpdateUser from "./update.user";
import { logError } from "@/utils";

const UserTablePage = ({
  users,
  meta,
}: {
  users: IUsersData[] | [];
  meta: IMetaUsers;
}) => {
  const router = useRouter();

  // const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = router;
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState<boolean>(false);
  const [dataUpdate, setDataUpdate] = useState<any>(null);
  const columns: ColumnType<IUsersData>[] = [
    {
      title: "Id",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "Actions",
      key: "actions",
      render: (_, record) => (
        <Space size="middle">
          <Button
            onClick={() => {
              setIsUpdateModalOpen(true);
              setDataUpdate(record);
            }}
          >
            Edit
          </Button>
          <Popconfirm
            title="Are you sure you want to delete this user?"
            onConfirm={() => handleDelete(record.id)}
          >
            <Button type="primary" danger>
              Delete
            </Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    if (users) {
      setIsFetching(false);
    }
  }, [users]);

  const handleDelete = async (userId: string) => {
    try {
      await handleDeleteUserAction({ id: userId });
      //   setUsers(users.filter((user) => user.id !== userId));
    } catch (error) {
      logError(`Error deleting user ${userId}:`, error);
    }
  };

  const onChange = (pagination: any) => {
    if (pagination && pagination.current) {
      replace(`${pathname}?page=${pagination.current}&limit=${meta.pageSize}`);
      setIsFetching(true);
    }
  };

  const renderTitle = () => {
    return (
      <div className="flex justify-end mb-4">
        <Button type="primary" onClick={() => setIsModalVisible(true)}>
          Add New User
        </Button>
      </div>
    );
  };

  return (
    <div>
      <style>
        {`.flex {
          display: flex;
        }
        .justify-end {
          justify-content: flex-end;
        }
        .mb-4 {
          margin-bottom: 1rem;
        }`}
      </style>

      {/* end of add new user button */}
      <Table
        title={renderTitle}
        loading={isFetching}
        key={"id"}
        dataSource={users || []}
        columns={columns}
        rowKey="id"
        pagination={{
          ...meta,
          pageSizeOptions: ["5", "10", "20"],
        }}
        onChange={onChange}
      />

      <CreateUser
        visible={isModalVisible}
        onCancel={() => setIsModalVisible(false)}
      />

      <UpdateUser
        isUpdateModalOpen={isUpdateModalOpen}
        setIsUpdateModalOpen={setIsUpdateModalOpen}
        dataUpdate={dataUpdate}
        setDataUpdate={setDataUpdate}
      />
    </div>
  );
};

export default UserTablePage;
