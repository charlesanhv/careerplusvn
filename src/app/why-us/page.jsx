import { BLOGS } from "@/constanst/constanst";
import styles from "./styles.module.css";
import Image from "next/image";
// const NUMPAGE = [1, 2, 3, 4];

export const metadata = {
  title: 'Grow Green Why Us - Grow Green Vietnam',
  openGraph: {
    title: 'Grow Green Why Us - Grow Green Vietnam',
  },
}
const WhyUs = async () => {
  return (
    <div className={styles.blog_container}>
      {/* <Heading
        title="WhyUs"
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique."
      /> */}

      <div className={styles.blogs_wrapper}>
        <div className={styles.blogs}>
          {BLOGS.map((item, index) => {
            return (
              <>
                <div
                  className={styles.blog_item}
                  key={index + item.id}
                  style={{
                    flexDirection: index % 2 === 0 ? "row-reverse" : "row",
                  }}
                >
                  <div className={styles.blog_img_wrapper}>
                    <Image
                      src={item?.image}
                      className={styles.blog_img}
                      alt="image"
                      width={500}
                      height={500}
                    />
                  </div>
                  <div className={styles.blog_content_wrapper}>
                    <div className={styles.blog_title}>{item?.title}</div>
                    <div className={styles.blog_description}>{item?.desc}</div>
                  </div>
                </div>
              </>
            );
          })}
        </div>

        {/* <BlogSidebar /> */}
      </div>
    </div>
  );
};

export default WhyUs;
