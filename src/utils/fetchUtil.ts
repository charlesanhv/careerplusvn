"use server";

import { logError } from ".";
import { refreshToken, getAuthToken } from "./authService";

// Define a cache object for storing responses

export async function fetchAPI(
  url: string,
  method = "GET",
  data?: any,
  next: Record<string, any> = {},
  headers?: HeadersInit,
  isPrivate = false
) {
  const initialHeaders: Record<string, string> = {
    "Content-Type": "application/json",
    ...(headers as Record<string, string>),
  };

  if (isPrivate) {
    const authToken = getAuthToken();
    if (authToken) {
      initialHeaders["Authorization"] = `Bearer ${authToken}`;
    }
  }

  const fetchOptions: RequestInit = {
    method,
    next,
    headers: initialHeaders,
    body: data ? JSON.stringify(data) : undefined,
  };

  try {
    const response = await fetch(url, fetchOptions);

    // Handle 401 Unauthorized explicitly
    if (response.status === 401 && isPrivate) {
      logError("401 Unauthorized - attempting to refresh token.");
      const newToken = await refreshToken(); // Attempt to refresh token
      if (newToken) {
        initialHeaders["Authorization"] = `Bearer ${newToken}`; // Update the Authorization header with the new token
        return fetchAPI(url, method, next, data, headers, isPrivate); // Retry the request with the new token
      } else {
        throw new Error(
          "Authentication failed after attempting to refresh token"
        );
      }
    }

    if (!response.ok) {
      const errorText = await response.text();
      logError(
        `Failed to fetch: ${url}, Status: ${response.status}, Error: ${errorText}`
      );
      throw new Error(
        `HTTP error! status: ${response.status}, ${response.statusText}`
      );
    }
    const responseData = await response.json();

    return responseData;
  } catch (error) {
    logError("Error getting users:", error);
    throw error; // Rethrowing the error for the caller to handle
  }
}
