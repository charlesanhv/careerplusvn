import clsx from "clsx";
import styles from "./TeachingApproach.module.css";
import Image from "next/image";
import Link from "next/link";

const stringText = `Established in 2014, Grow Green Vietnam began as a modest
                  charitable initiative with a fervent mission to provide free
                  English lessons to underprivileged children in Hanoi. From its
                  inception, the organization’s primary objective has been to
                  cultivate a profound appreciation for the English language and
                  global cultures among Vietnam's youth, thereby promoting a
                  more interconnected and globally conscious society.`;

const TeachingApproach = ({ type = "home" }) => {
  return (
    <div className={styles.comprehensive_teaching_approach}>
      <div className={styles.content_wrapper}>
        <div className={clsx("container")}>
          <div className={clsx("row", styles.fullWidthSmallScreen)}>
            <div
              className={clsx(
                "wrapper",
                styles.comprehensive_teaching_approach_content
              )}
              style={{ alignItems: "center" }}
            >
              <div className={styles.comprehensive_teaching_approach_image}>
                <Image
                  src={
                    type === "home"
                      ? "/images/dsc00445.webp"
                      : "/images/about/cp-2.webp"
                  }
                  style={{ width: "100%", height: "auto" }}
                  alt="image"
                  width={500}
                  height={500}
                />
              </div>

              <div className={styles.comprehensive_teaching_approach_text}>
                <span className={clsx(styles.content_title, "title")}>
                  {type === "about"
                    ? "About Us"
                    : "A comprehensive teaching career"}
                </span>
                <span
                  className={clsx(
                    styles.content_description,
                    styles.first_description
                  )}
                >
                  {stringText}
                </span>
                <span
                  className={clsx(
                    styles.content_description,
                    styles.first_description
                  )}
                >
                  {type === "about" &&
                    `Over the past decade, Grow Green Vietnam has experienced substantial growth. We have established a dedicated English learning center that not only offers high-quality language education but also serves as a hub for collaboration with various language centers, public and international schools in Hanoi and neighboring cities. This expansion reflects our unwavering commitment to making English education accessible to all children, empowering them to enhance their language proficiency and unlock new opportunities. True to our name, Grow Green Vietnam is dedicated to fostering long term growth both for our students and educators by cultivating enduring partnerships with schools and language centers throughout the region.
                 `}
                </span>
                {type === "about" && (
                  <span
                    className={clsx(
                      styles.content_description,
                      styles.first_description
                    )}
                  >
                    Today, Grow Green Vietnam proudly comprises a dynamic team
                    of over 200 teachers who contribute significantly to the
                    educational advancement of countless children while enjoying
                    a fulfilling lifestyle that blends meaningful work with the
                    opportunity to explore and experience life in Vietnam.
                  </span>
                )}
                {type === "home" && (
                  <Link
                    href="/about"
                    className={styles.Learn_more_text}
                    style={{ textDecoration: "none" }}
                  >
                    Learn more
                  </Link>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TeachingApproach;
