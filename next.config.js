/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    images: {
        domains: ['picsum.photos', 'i.pravatar.cc'],
        unoptimized: true,
        loader: "default",

    },
};

module.exports = nextConfig;
