import clsx from "clsx";
import styles from "./ProgramOffer.module.css";
import { PROGRAMS } from "@/constanst/constanst";
import Image from "next/image";
import Link from "next/link";


// eslint-disable-next-line react/prop-types
const ProgramOffer = ({data=PROGRAMS}) => {
    return (
        <div className={clsx(styles.content_wrapper, styles.program_we_offer)}>
        <span className={clsx(styles.content_title,'title')}>{data?.title}</span>
        <span
          className={clsx(
            styles.content_description,
            styles.programs_description
          )}
        >
          {data?.description}
        </span>

        <div className={styles.programs_container}>
          {data?.data?.map((item, index) => (
            <div className={styles.programs} key={index}>
              <div className={styles.programs_img_wrapper}>
                <Image
                  unoptimized
                  src={item.img}
                  className={styles.programs_img}
                  alt="image"
                  width={200}
                  height={200}
                />
              </div>
              <span className={styles.programs_title}>{item.title}</span>
              <span
                className={clsx(styles.content_description)}
                style={{ marginBottom: "20px" }}
              >
                {item.des}
              </span>
              <Link 
                href={`/jobs/${item.slug}`}
                className={clsx(
                  styles.Learn_more_text,
                  styles.programs_learn_more
                )}
                style={{textDecoration: 'none'}}
              >
                Learn more
              </Link>
            </div>
          ))}
        </div>
      </div>

    )
    
}

export default ProgramOffer