"use client";
import React from "react";
import { useRouter } from 'next/navigation'
import { Button, Form, Input } from "antd";
import { log } from "@/utils";

type FieldType = {
  username?: string;
  password?: string;
  remember?: string;
};

const LoginForm = () => {
  const router = useRouter();
  const onFinish = (values: any) => {
    log("Success:", values);
    if (values.username === "anhvt" && values.password === "Anhvt@25") {
      alert("Login success");
      router.push("/users");
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    log("Failed:", errorInfo);
    alert("Login failed");
  };

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600, marginTop: "50px" }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="Username"
        name="username"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input />
      </Form.Item>

      <Form.Item<FieldType>
        label="Password"
        name="password"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginForm;
