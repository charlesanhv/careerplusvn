'use client'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './BackToTop.module.css'
import { faArrowUpLong } from '@fortawesome/free-solid-svg-icons'

const BackToTop = () => {


    
    return (
        <div
            className={styles.back_to_top}
            onClick={() => {
                window.scrollTo({ top: 0, behavior: 'smooth' });
            }}
            
        >
          <FontAwesomeIcon icon={faArrowUpLong} />
        </div>
    )
}

export default BackToTop