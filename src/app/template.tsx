'use client'

import React from 'react'
import { motion } from 'framer-motion'

export default function Transition({ children }: { children: React.ReactNode }) {
  const spring = {
    // type: 'spring',
    // damping: 10,
    // stiffness: 100
    ease: 'easeInOut',
    duration: 0.5
  }
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring} className='112'>
      {children}
    </motion.div>
  )
}
