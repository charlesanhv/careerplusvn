export interface JobDetailProps {
  slug: string;
}
export interface JobDescription {
  studentDemographics: string;
  classSize: string;
  curriculumSupport?: string;
  workingSchedule?: string;
  workHours?: string;
}
export interface JobBenefits {
  professionalDevelopment: string;
  competitiveCompensation: string;
  accommodationAndTransportation: string;
}
export interface JobRequirements {
  qualifications: string;
  certification: string;
  experience: string;
}
export interface JobData {
  slug: string;
  title: string;
  location: string;
  description: JobDescription;
  benefits: JobBenefits;
  requirements: JobRequirements;
}
