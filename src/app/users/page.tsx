import { handleGetUsersAction } from "@/action";
import UserTablePage from "@/components/users/users.table";
import styles from "./users.module.css";

// const calculatePagesCount = (pageSize: number, totalCount: number) => {
//   return totalCount < pageSize ? 1 : Math.ceil(totalCount / pageSize);
// };
const UsersPage = async (props: any) => {
  const LIMIT = 5;
  const page = props?.searchParams?.page ?? 1;
  const response = await handleGetUsersAction(page, LIMIT);
  const total_items = 20;
  // const totalPages = calculatePagesCount(LIMIT, Number(total_items));
  return (
    <>
      <div className={styles.one}>
        <h1>User List</h1>
      </div>
      <UserTablePage
        users={response}
        meta={{
          current: +page,
          pageSize: LIMIT,
          total: Number(total_items),
        }}
      />
    </>
  );
};

export default UsersPage;
