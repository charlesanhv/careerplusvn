import TeachingApproach from "../Components/TeachingApproach/TeachingApproach";
import TopStories from "../Components/TopStories/TopStories";
import TrainingProgram from "../Components/TrainingProgram/TrainingProgram";

export const metadata = {
    title: 'Introduction to GrowGreen Tutoring Center | growgreenvietnam.com',
    openGraph: {
      title: 'GrowGreen Tutoring Center is a combination of information technology and at-home tutoring services, providing the highest level of user experience and information security',
    },
  }

const About =  async () => {
    return (
       <>
       
       {/* <Heading title="" description="" /> */}
       <TeachingApproach type="about"/>
       {/* <EssentialResources /> */}
       <TopStories type="about" />
        {/* <OurPartners /> */}
        <TrainingProgram />
       </>
    );
}

export default About