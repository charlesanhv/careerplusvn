"use client";

export default function LoadingPage() {
  return (
    <div className="flex h-screen flex-col items-center justify-center bg-black">
      <div className="loadingSkeletonCustom">
        <div className="loadingSkeletonCustom_item"></div>
        <div className="loadingSkeletonCustom_item"></div>
        <div className="loadingSkeletonCustom_item"></div>
      </div>
    </div>
  );
}
