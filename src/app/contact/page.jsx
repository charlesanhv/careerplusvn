import Heading from "../Components/Heading/Heading";
import styles from "./styles.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEnvelope,
  faHome,
  faTablet,
} from "@fortawesome/free-solid-svg-icons";
import Form from "../Components/form/Form";

const CONTACT_INFO = [
  {
    icon: faHome,
    title: "Hanoi, Vietnam",
    info: "25 Hoa Bang Street, Yen Hoa ward, Cau Giay district",
  },

  {
    icon: faTablet,
    title: "(+84) 962 501 336",
    info: "Mon to Sat 9am to 6pm",
  },

  {
    icon: faEnvelope,
    title: "tuyendung@growgreenvietnam.com",
    info: "Send us your query anytime!",
  },
];

export const metadata = {
  title: 'Grow Green Contact Page - Grow Green Vietnam',
  openGraph: {
    title: 'Grow Green Contact Page - Grow Green Vietnam',
  },
}

const Contact = async () => {
  return (
    <>
      <Heading
        title=""
        description=""
      />

      <div className={styles.contact_content_container}>
        <div className={styles.contact_content_wrapper}>
          <div className={styles.contact_content_title}>Get in Touch</div>
          <div className={styles.contact_content}>
            <Form />
            <div className={styles.web_contact_info}>
              {CONTACT_INFO.map((item, index) => {
                return (
                  <div className={styles.web_contact_info_item} key={index}>
                    <FontAwesomeIcon
                      icon={item.icon}
                      color="#8f9195"
                      size="xl"
                    />

                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <div className={styles.web_contact_info_item_title}>
                        {item.title}{" "}
                      </div>

                      <div className={styles.web_contact_info_item_description}>
                        {item.info}{" "}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Contact;
