import React, { useState } from "react";
import { Modal, Form, Input, message } from "antd";
import styles from "./styles.module.css";
import { handleCreateUserAction } from "@/action";

interface CreateUserModalProps {
  onCancel?: () => void;
  visible: boolean;
}

const CreateUser = ({ onCancel, visible }: CreateUserModalProps) => {
  const [form] = Form.useForm();
  const [isLoading, setLoading] = useState<boolean>(false);
  const handleOk = () => {
    form.submit();
  };

  const handleCancel = () => {
    if (onCancel) {
      onCancel();
    }
  };

  const onFinish = async (values: any) => {
    setLoading(true);
    const res = await handleCreateUserAction(values);
    if (res) {
      if (onCancel) {
        message.success("User created successfully");
        onCancel();
        setLoading(false);
      }
    }
  };

  return (
    <Modal
      title="Create User"
      open={visible}
      onOk={handleOk}
      confirmLoading={isLoading}
      onCancel={handleCancel}
      className={styles.createUserModal}
    >
      <Form
        form={form}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "350px" }}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, type: "email" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Name" name="name" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default CreateUser;
