import type { Metadata } from "next";
import { Chivo } from "next/font/google";
import { GoogleTagManager, GoogleAnalytics } from "@next/third-parties/google";
import StyledComponentsRegistry from "@/lib/antd.registry";
import { keywords } from "@/config/keywords.config";
import Header from "@/components/header";
import Footer from "@/components/Footer";
import "./global.css";
import BackToTop from "@/components/BackToTop/BackToTop";
// import { WebVitals } from "./_components/web-vitals";
import { Analytics } from "@vercel/analytics/react";

const chivo = Chivo({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Grow Green Native Teacher Supply",
  description: "Grow Green Vietnam is a Vietnamese Native Teacher Supply Company that provides high-quality native language instruction to Vietnamese students.",
  applicationName: "Grow Green Vietnam",
  keywords: keywords,
  authors: [{ name: "Grow Green Vietnam" }],
  creator: "Grow Green Vietnam",
  themeColor: "#fff",
  viewport: "width=device-width, initial-scale=1",
  robots: {
    index: true,
    follow: true,
  },
  category: "Education, Language, Teacher, Training, Vietnam",
  publisher: "Grow Green Vietnam",
  formatDetection: {
    telephone: false,
    address: false,
    email: false,
  },
  openGraph: {
    title: "Grow Green Native Teacher Supply",
    description: "Growgreen Viet Nam is a Vietnamese Native Teacher Supply Company that provides high-quality native language instruction to Vietnamese students.",
    url: "https://growgreenvietnam.com",
    siteName: "Grow Green Vietnam",
    locale: "en-US",
    type: "website",
    images: [
      {
        url: "https://growgreenvietnam.com/images/bg_image.png",
        width: 1200,
        height: 630,
        alt: "Growgreenvietnam",
        type: "image/jpeg",
      },
    ],
  },
  twitter: {
    title: "Grow Green Native Teacher Supply",
    card: "summary_large_image",
    creator: "@growgreenvietnam",
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <GoogleTagManager gtmId="GTM-K7C8PQMM" />
      <body className={`${chivo.className}`}>
        <StyledComponentsRegistry>
          {/* <WebVitals /> */}
          <Analytics />
          <Header />
          {children}
          <BackToTop />
          <Footer />
        </StyledComponentsRegistry>
      </body>
      <GoogleAnalytics gaId="G-6E7HXFJKF7" />
    </html>
  );
}
