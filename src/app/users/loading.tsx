"use client";

import { Skeleton } from "antd";

const LoadingSkeleton = () => {
  return (
    <div className="flex flex-col w-full p-4 rounded-lg shadow-md bg-white">
      <Skeleton active className="w-full" />
    </div>
  );
};

export default LoadingSkeleton;
