"use client";
import clsx from "clsx";
import styles from "./Form.module.css";

const Form = () => {
  // const handleAction = async () => {
  // };
  return (
    <div className={styles.form} style={{ width: "100%" }}>
      <div className={styles.form_text_area_container}>
        <textarea placeholder={""} className={styles.form_textarea} />
      </div>
      <div
        className={clsx(styles.form_input_container, styles.form_double_input)}
      >
        <input placeholder={""} className={styles.form_input_half_width} />
        <input placeholder={""} className={styles.form_input_half_width} />
      </div>

      <div className={styles.form_input_container}>
        <input placeholder={""} className={styles.form_input} />
      </div>

      <div className={styles.form_button_container}>
        <a href="mailto:tuyendung@growgreenvietnam.com" style={{ cursor: "pointer", textDecoration: "none"}}>Send</a>
      </div>
    </div>
  );
};

export default Form;
