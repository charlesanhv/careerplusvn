import styles from "./styles.module.css";
import { COMMENTS, dataBogDetails } from "@/constanst/constanst";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faComment,
  faHeart,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import {
  faBehance,
  faDribbble,
  faFacebookF,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

import clsx from "clsx";

import Image from "next/image";
import BlogSidebar from "../BlogSidebar/BlogSidebar";
import Form from "../form/Form";
import Marquee from "react-fast-marquee";

const ICON = [
  {
    icon: faFacebookF,
  },
  {
    icon: faTwitter,
  },
  {
    icon: faDribbble,
  },
  {
    icon: faBehance,
  },
];

const BlogDetail = ({ slug }: { slug: string }) => {
  const dataDetail: any = dataBogDetails.find((job) => job.slug === slug);
  // Dữ liệu tác giả (bạn cần thay thế bằng dữ liệu thực tế)
  const author = {
    avatar: "/images/author.png.webp",
    name: "Harvard Milan",
    bio: "An experienced educator passionate about sharing knowledge and fostering a love of learning. Dedicated to creating engaging and effective learning experiences for students of all backgrounds.",
  };
  return (
    <>
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "start",
          marginTop: "70px",
        }}
      >
        <div className={clsx("container", styles.blog_details_container)}>
          <div className={clsx("row", styles.blog_details_content)}>
            <div className={styles.blog_details_content_wrapper}>
              <div style={{ padding: "0 10px" }}>
                {/* Hiển thị danh sách hình ảnh */}
                <div className={styles.image_list}>
                  <Marquee>
                    {dataDetail?.listImages?.map((image: any, index: any) => (
                      <div key={index} className={styles.image_item}>
                        <Image
                          src={image.url}
                          alt="image"
                          width={200} // Điều chỉnh kích thước theo nhu cầu
                          height={200} // Điều chỉnh kích thước theo nhu cầu
                        />
                      </div>
                    ))}
                  </Marquee>
                </div>

                <div className={styles.blog_details_title}>
                  {dataDetail?.title}
                </div>

                {/* Hiển thị thông tin tác giả và bình luận (nếu có) */}
                <div className={styles.blog_info_link}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      gap: "10px",
                    }}
                  >
                    <FontAwesomeIcon icon={faUser} color="#999999" />
                    <span className={styles.link_text}>Admin</span>
                  </div>

                  <div className={styles.divider} />

                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      gap: "10px",
                    }}
                  >
                    <FontAwesomeIcon icon={faComment} color="#999999" />
                    <span className={styles.link_text}>00 Comments</span>
                  </div>
                </div>

                {/* Hiển thị nội dung dựa trên loại slug */}
                {dataDetail.slug ===
                  "reasons-choosing-vietnam-as-your-destination" &&
                  dataDetail?.sections?.map((section: any, index: any) => (
                    <div key={index}>
                      <h2 className={styles.section_heading}>
                        {section.heading}
                      </h2>
                      <p className={styles.blog_details_text}>
                        {section.content}
                      </p>
                    </div>
                  ))}

                {dataDetail.slug === "staying-in-vietnam" && (
                  <div>
                    {/* Hiển thị introduction */}
                    <p className={styles.blog_details_text}>
                      {dataDetail?.sections?.[0]?.introduction}
                    </p>

                    {/* Hiển thị categories */}
                    {dataDetail?.sections?.[0]?.categories?.map(
                      (category: any, index: any) => (
                        <div key={index}>
                          <h3 className={styles.category_heading}>
                            {category.name}
                          </h3>
                          <ul>
                            {category.items.map((item: any, itemIndex: any) => (
                              <li key={itemIndex}>
                                <strong className={styles.item_name}>
                                  {item.name}:
                                </strong>{" "}
                                {item.description}
                              </li>
                            ))}
                          </ul>
                        </div>
                      )
                    )}

                    {/* Hiển thị conclusion */}
                    <p className={styles.blog_details_text}>
                      {dataDetail?.sections?.[0]?.conclusion}
                    </p>
                  </div>
                )}

                {/* Phần còn lại của component (post_nav_wrapper, cmt_area, cmt_form_area) */}
                <div className={styles.post_nav_wrapper}>
                  <div className={styles.like_social}>
                    <p className={styles.like_info}>
                      <span
                        style={{
                          marginRight: 5,
                          verticalAlign: "middle",
                          paddingRight: 5,
                        }}
                      >
                        <FontAwesomeIcon
                          icon={faHeart}
                          color="#5D646A"
                          size="lg"
                        />
                      </span>
                      GrowGreen and 4 people like this
                    </p>

                    <span className={styles.social_icon}>
                      {ICON.map((item, index) => (
                        <FontAwesomeIcon
                          icon={item.icon}
                          color="#999999"
                          key={index}
                        />
                      ))}
                    </span>
                  </div>

                  {/* <div className={styles.nav_area}>
                    <div className={styles.nav_box}>
                      <div className={styles.thumpnail}>
                        <FontAwesomeIcon
                          icon={faArrowLeft}
                          className={styles.icon_arrow}
                          size="lg"
                        />
                      </div>

                      <div style={{ display: "block", textAlign: "left" }}>
                        <p className={styles.nav_text}>Prev Post</p>
                        <p className={styles.nav_title}>
                          Space The Final Frontier
                        </p>
                      </div>
                    </div>

                    <div
                      className={styles.nav_box}
                      style={{ justifyContent: "flex-end" }}
                    >
                      <div style={{ display: "block", textAlign: "left" }}>
                        <p
                          className={styles.nav_text}
                          style={{ textAlign: "right" }}
                        >
                          Next Post
                        </p>
                        <p className={styles.nav_title}>
                          Space The Final Frontier
                        </p>
                      </div>

                      <div
                        className={clsx(
                          styles.thumpnail,
                          styles.thumpnail_right
                        )}
                      >
                        <FontAwesomeIcon
                          icon={faArrowRight}
                          className={styles.icon_arrow}
                          size="lg"
                        />
                      </div>
                    </div>
                  </div> */}

                  <div className={styles.blog_author_area}>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <Image
                        className={styles.blog_author_img}
                        src={author.avatar}
                        alt="image"
                        width={50}
                        height={50}
                      />
                      <div>
                        <div className={styles.blog_name}>{author.name}</div>
                        <div className={styles.blog_cmt}>{author.bio}</div>
                      </div>
                    </div>
                  </div>

                  <div className={styles.cmt_area}>
                    <div className={styles.cmt_title}>{COMMENTS.length} Comments</div>
                    {COMMENTS.map((item, index) => (
                      <div key={index} className={styles.cmt_item}>
                        <div className={styles.cmt_img}>
                          <Image
                            src={item.avatar}
                            alt="image"
                            width={70}
                            height={70}
                          />
                        </div>
                        <div>
                          <div className={styles.blog_cmt} style={{ marginBottom: 10 }}>
                            {item.content}
                          </div>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                            }}
                          >
                            <div style={{ display: "flex", alignItems: "center" }}>
                              <span className={styles.blog_name}>{item.name}</span>
                              <span className={styles.time_cmt}>{item.time}</span>
                            </div>
                            <div className={styles.replay_btn}>
                              <span className={styles.replay_text}>Reply</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>

                  <div className={styles.cmt_form_area}>
                    <div className={styles.cmt_title}>Leave a Reply</div>
                    <Form />
                  </div>
                </div>
              </div>
            </div>
            <BlogSidebar />
          </div>
        </div>
      </div>
    </>
  );
};

export default BlogDetail;
